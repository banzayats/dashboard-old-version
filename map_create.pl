#!/usr/bin/perl

use JSON::RPC::Client;
use Data::Dumper;
use utf8;
use open qw(:std :utf8);

my $debug = 1;
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';
my $authID;
my $response;

my $json = {
    jsonrpc => "2.0",
    method => "user.login",
    params => {
        user => "zabbix",
        password => "zabbix"
    },
    id => 1
};
undef $response;
while ( ! defined $response ) {
    $response = $client->call($url, $json);
}
die "Authentication failed\n" unless $response->content->{'result'};
$authID = $response->content->{'result'};

$json = {
    jsonrpc=> '2.0',
    method => 'map.create',
    params => {
        name => "TEST",
        width => 800,
        height => 400,
    },
    id => 2,
    auth => "$authID",
};
undef $response;
while ( ! defined $response ) {
    $response = $client->call($url, $json);
}
die "trigger.get failed\n" unless $response->content->{result};

print Dumper($response);