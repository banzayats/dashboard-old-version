#!/usr/bin/perl

use JSON::RPC::Client;
use Data::Dumper;
use utf8;
use open qw(:std :utf8);

my $sysmapid = 14;
my $name     = "Test";

my $debug = 1;
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';
my $authID;
my $response;
my @map_elements;
my @map_links;

my $json = {
    jsonrpc => "2.0",
    method => "user.login",
    params => {
        user => "zabbix",
        password => "zabbix"
    },
    id => 1
};
undef $response;
while ( ! defined $response ) {
    $response = $client->call($url, $json);
}
die "Authentication failed\n" unless $response->content->{'result'};
$authID = $response->content->{'result'};

@map_elements = [
    {
        elementid   => 10907,
        selementid  => 1,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 0,
        y => 30,
    },
    {
        elementid   => 10985,
        selementid  => 2,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 100,
        y => 30,
    },
    {
        elementid   => 10846,
        selementid  => 3,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 200,
        y => 30,
    },
    {
        elementid   => 10579,
        selementid  => 4,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 300,
        y => 30,
    },
    {
        elementid   => 10381,
        selementid  => 5,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 400,
        y => 30,
    },
    {
        elementid   => 10571,
        selementid  => 6,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 500,
        y => 30,
    },
    {
        elementid   => 10872,
        selementid  => 7,
        elementtype => 0,
        iconid_off => 44,
        label => '{HOST.NAME}',
        x => 600,
        y => 30,
    },
    {
        elementid   => 10906,
        selementid  => 8,
        elementtype => 0,
        iconid_off => 11,
        label => '{HOST.NAME}',
        x => 0,
        y => 200,
    }, 
    {
        elementid   => 10984,
        selementid  => 9,
        elementtype => 0,
        iconid_off => 11,
        label => '{HOST.NAME}',
        x => 100,
        y => 200,
    },
];

@map_links = [
    {
        selementid1 => 1,
        selementid2 => 8,
    },
    {
        selementid1 => 2,
        selementid2 => 9,
    },
];

$json = {
    jsonrpc=> '2.0',
    method => 'map.update',
    params => {
        sysmapid => $sysmapid,
        name => $name,
        width => 1200,
        height => 600,
        selements => @map_elements,
	links => @map_links,
    },
    id => 2,
    auth => "$authID",
};

print Dumper($json);

undef $response;
while ( ! defined $response ) {
    $response = $client->call($url, $json);
}
die "map.update failed\n" unless $response->content->{result};

print Dumper($response);
