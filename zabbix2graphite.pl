#!/usr/bin/perl

use JSON::RPC::Client;
use Net::Graphite;
use Data::Dumper;
use Lingua::Translit;
use utf8;
use open qw(:std :utf8);

my $template = shift or die "Необходим аргумент - имя темплейта\n";

my $debug = 1;
my $id = 4;
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';
my $authID;
my $response;
my $number_of_templates;
my @templateids;
my @hostids;

my $graphite = Net::Graphite->new(
    host => '10.10.10.10',
    port => 2003,
    trace => 0,            # if true, copy what's sent to STDERR
    proto => 'tcp',        # can be 'udp'
    timeout => 1,          # timeout of socket connect in seconds
    fire_and_forget => 0,  # if true, ignore sending errors
);

my $json = {
    jsonrpc => "2.0",
    method => "user.login",
    params => {
        user => "zabbix",
        password => "zabbix"
    },
    id => 1
};
$response = $client->call($url, $json);
die "Authentication failed\n" unless $response->content->{'result'};
$authID = $response->content->{'result'};

while(1){

$json = {
    jsonrpc=> '2.0',
    method => 'template.get',
    params => {
        output => ['templateids','name'],
        search => {
            host => $template
        }
    },
    id => 2,
    auth => "$authID",
};
$response = $client->call($url, $json);
die "template.get failed\n" unless $response->content->{result};
foreach my $template (@{$response->content->{result}}) {
    print "Шаблон: ".$template->{name}."\n" if $debug;
    $number_of_templates = push (@templateids, $template->{templateid});
}

$json = {
    jsonrpc=> '2.0',
    method => 'host.get',
    params => {
        output => ['hostid','name'],# get only host id and host name
        sortfield => 'name',        # sort by host name
        filter => {
            status => 0             # get only active hosts
        },
        templateids => @templateids,
    },
    id => 3,
    auth => "$authID",
};
$response = $client->call($url, $json);
die "host.get failed\n" unless $response->content->{result};
foreach my $host (@{$response->content->{result}}) {
    print "Хост: ".$host->{name}."\n" if $debug;
    $json = {
        jsonrpc=> '2.0',
        method => 'item.get',
        params => {
            output => 'extend',
            sortfield => 'name',
            hostids => $host->{hostid},
            filter => {
                status => 0
            },
        },
        id => $id,
        auth => "$authID",
    };
    $host->{name} =~ s/ /_/g;
    $response = $client->call($url, $json);
    die "item.get failed\n" unless $response->content->{result};
    foreach my $item (@{$response->content->{result}}) {
        $item->{key_} =~ s/\./_/g;
        my $path = "Zabbix_monitoring.".$host->{name}.".".$item->{key_};
        print "$path $item->{lastvalue} $item->{lastclock}\n" if $debug;
        $graphite->send(
            path => $path,
            value => $item->{lastvalue},
            time => $item->{lastclock},
        );
    }
    $id++;
}
sleep 30;
}