#!/usr/bin/perl

use Modern::Perl;
use Data::Dumper;
use HTTP::Date;
use SOAP::Lite( 'autodispatch', proxy => 'http://otrs.com/otrs/rpc.pl' );

my $User = 'user';
my $Pw   = 'pa$$w0rd';
my $RPC = Core->new();

my @TicketEscalated = $RPC->Dispatch( $User, $Pw, 'TicketObject', 'TicketSearch', Result => 'ARRAY', QueueIDs => [5, 10], States   => ['new', 'open'], TicketEscalationTimeOlderMinutes => -60, UserID => 1);

my $count = @TicketEscalated;

if ($count > 0){
    say "Created;Title;TicketID";
    foreach (@TicketEscalated){
        my %Ticket = $RPC->Dispatch( $User, $Pw, 'TicketObject', 'TicketGet', , TicketID => $_, UserID => 1);
        $Ticket{Title} =~ s/"//g;
        say join(';', $Ticket{Created}, $Ticket{Title}, $Ticket{TicketID} );
        #print Dumper(%Ticket);
    }
}
else {
    say "Created;Title;TicketID";
    say "-;-;-";
}
