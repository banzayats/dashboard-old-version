#!/usr/bin/perl

use Getopt::Std;
use JSON::RPC::Client;
use IO::Socket::INET;
use Data::Dumper;
use utf8;
use open qw(:std :utf8);
use Time::Local;
use POSIX;

my $json;
my $time;
my $authID;
my $response;
my @hostids;

my $debug = 1;
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';

my %params = undef;
getopt('itd', \%params);
my $itemid = $params{'i'};
my $type = $params{'t'};
my $date = $params{'d'};
die "Необходим параметр -i (itemid)\n" unless $itemid;
die "Необходим параметр -t (type)\n" unless length($type);
die "Необходим параметр -d (date)\n" unless $date;

my($year, $month, $day, $hour, $minute, $second);
if($date =~ m{^\s*(\d{1,4})\W*0*(\d{1,2})\W*0*(\d{1,2})\W*0*(\d{0,2})\W*0*(\d{0,2})\W*0*(\d{0,2})}x) {
    die "Введите дату в формате ГГГГ-ММ-ДД [ЧЧ-ММ-СС]\n" if $2 > 12;
    $year = $1; $month = $2; $day = $3; $hour = $4; $minute = $5; $second = $6;
    $hour |= 0;  $minute |= 0;  $second |= 0;  # defaults
    $year = ($year<100 ? ($year<70 ? 2000+$year : 1900+$year) : $year);
    $date = timelocal($second,$minute,$hour,$day,$month-1,$year);
} else {
     die "Введите дату в формате ГГГГ-ММ-ДД [ЧЧ-ММ-СС]\n";
}

my $time_till = time;
my $periods = int(($time_till-$date)/30);
print "Количество элементов: $periods\n" if $debug;

my $sock = IO::Socket::INET->new(
    PeerAddr => '10.10.10.10',
    PeerPort => 2003,
    proto => 'tcp'
    );

die "Невозможно подключиться к Graphite: $!\n" unless $sock;

$json = {jsonrpc => "2.0",method => "user.login",params => {user => "zabbix",password => "zabbix"},id => 1};
$response = $client->call($url, $json);
die "Authentication failed\n" unless $response->content->{'result'};
$authID = $response->content->{'result'};

$json = {jsonrpc => '2.0',method => 'host.get',params => {output => ['hostid','name'],itemids => $itemid},id => 2,auth => "$authID"};
$response = $client->call($url, $json);
die "host.get failed\n" unless scalar(@{$response->content->{result}});
my $host = shift(@{$response->content->{result}});

$json = {jsonrpc => '2.0',method => 'item.get',params => {output => ['key_'],itemids => $itemid},id => 3,auth => "$authID"};
$response = $client->call($url, $json);
die "item.get failed\n" unless scalar(@{$response->content->{result}});
my $item = shift(@{$response->content->{result}});
$item->{key_} =~ s/\./_/g;

if ($periods < 1000) {
    historyget($type,$itemid,$date,$time_till);
} else {
    my $i_max = ceil(($time_till-$date)/30000);
    print "Now - $time_till; till - $date; i_max - $i_max\n";
    for ($i = 0; $i < $i_max; $i++) {
        $time_from = $time_till - 30000;
        if ($i == $i_max - 1 ) {
            print "$time_till-$date\n" if $debug;
            historyget($type,$itemid,$date,$time_till);
        } else {
            print "$time_till-$time_from\n" if $debug;
            historyget($type,$itemid,$time_from,$time_till);
        }
        $time_till -= 30000;
        sleep(2);
    }
}
$sock->shutdown(1);

print "Transport finished!\n";

sub historyget {
    $json = {
        jsonrpc => "2.0",
        method => "history.get",
        params => {
            output => 'extend',
            history => $_[0],
            itemids => $_[1],
            sortfield => 'clock',
            sortorder => 'DESC',
            time_from => $_[2],
            time_till => $_[3],
        },
        id => 50,
        auth => "$authID",
    };
    $response = $client->call($url, $json);
    die "history.get failed\n" unless scalar(@{$response->content->{result}});
    foreach my $historyitem (@{$response->result}) {
        my %h = %{$historyitem};
        my $path = "Zabbix_monitoring.".$host->{name}.".".$item->{key_};
        print "$path $h{'value'} $h{'clock'}\n" if $debug;
        $sock->send("$path $h{'value'} $h{'clock'}\n");
    }
}
