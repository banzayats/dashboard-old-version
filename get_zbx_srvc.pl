#!/usr/bin/perl

use JSON::RPC::Client;
use Data::Dumper;
use utf8;
use open qw(:std :utf8);

my $service = shift or die "Необходим аргумент - id сервиса\n";

# Authenticate yourself
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';
my $authID;
my $response;
my @hostids;

my $json = {
    jsonrpc => "2.0",
    method => "user.login",
    params => {
        user => "zabbix",
        password => "zabbix"
    },
    id => 1
};

$response = $client->call($url, $json);

# Check if response was successful
die "Authentication failed\n" unless $response->content->{'result'};

$authID = $response->content->{'result'};

$json = {
    jsonrpc=> '2.0',
    method => 'service.get',
    params => {
        output => 'extend',
        serviceids => $service,
    },
    id => 2,
    auth => "$authID",
};
$response = $client->call($url, $json);

# Check if response was successful
die "service.get failed\n" unless $response->content->{result};


foreach my $service (@{$response->content->{result}}) {
    print $service->{status};
}





