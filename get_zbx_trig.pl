#!/usr/bin/perl

use JSON::RPC::Client;
use Data::Dumper;
use utf8;
use open qw(:std :utf8);

my $groupid = shift or die "Необходим аргумент - groupid\n";

# Authenticate yourself
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';
my $authID;
my $response;
my @hostids;

my $json = {
    jsonrpc => "2.0",
    method => "user.login",
    params => {
        user => "zabbix",
        password => "zabbix"
    },
    id => 1
};

$response = $client->call($url, $json);

# Check if response was successful
die "Authentication failed\n" unless $response->content->{'result'};

$authID = $response->content->{'result'};

$json = {
    jsonrpc=> '2.0',
    method => 'host.get',
    params => {
        output => ['hostid','name'],# get only host id and host name
        sortfield => 'name',        # sort by host name
        filter => {
            status => 0             # get only active hosts
        },
        groupids => $groupid,       # hosts belong to groupid
    },
    id => 2,
    auth => "$authID",
};
$response = $client->call($url, $json);

# Check if response was successful
die "host.get failed\n" unless $response->content->{result};


foreach my $host (@{$response->content->{result}}) {
    my $number_of_hostids = push (@hostids, $host->{hostid});
}


$json = {
    jsonrpc=> '2.0',
    method => 'trigger.get',
    params => {
        hostids => {@hostids},
        filter => {
            value => 1		# get only triggers with problem
        },
        output => 'extend',
    },
    id => 3,
    auth => "$authID",
};
$response = $client->call($url, $json);

die "host.get failed\n" unless $response->content->{result};

foreach my $trigger (@{$response->content->{result}}) {
    print "triggerid: ".$trigger->{triggerid}." status: ".$trigger->{status}." comments: ".$trigger->{comments}."\n";
    }