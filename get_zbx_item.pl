#!/usr/bin/perl

use Modern::Perl;
use Zabbix::API;

my %cfg = (ip => '0.0.0.0', user => 'zabbix', pass => 'zabbix');
my $optarg = shift or die "hostname:item.key must be an argument\n";
my ($hostname, $itemkey) = split /:/,$optarg;
my $zabbix = Zabbix::API->new(server=>'http://'.$cfg{ip}.'/zabbix/api_jsonrpc.php',verbosity=>0 );

$zabbix->login(user => $cfg{user}, password=>$cfg{pass});

my $host = shift @{$zabbix->fetch('Host', params=>{filter=>{host=>$hostname}})};
my $item = shift @{$zabbix->fetch('Item', params=>{hostids=>$host->id(), search=>{key_=>$itemkey}})};
my $data = $item->data();
print $data->{'lastvalue'};