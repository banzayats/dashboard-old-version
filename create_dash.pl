#!/usr/bin/perl

use Modern::Perl;
use Config::IniFiles;
use HTML::Template;
use Text::CSV::Slurp;
use Data::Dumper;
use Proc::Daemon;
use encoding 'utf8';

my $home = "/opt/zabbix";
my $cfg = Config::IniFiles->new( -file => "$home/dash.ini" ) or die "Не могу открыть файл конфигурации.\n";
my $template = HTML::Template->new(filename => "$home/dash.tmpl", utf8 => 1) or die "Не могу открыть файл шаблона.\n";
my $filename = "/var/www/html/dashboard.html";
my ($cmd, $csvstring);

Proc::Daemon::Init;

my $continue = 1;
$SIG{TERM} = sub { $continue = 0 };

while ($continue) {

# tickets
$cmd = $cfg->val('tickets', 'new');
$csvstring = `perl $home/$cmd`;
my $tbl_new = Text::CSV::Slurp->load(string=>$csvstring, sep_char=>';');
$template->param('new' => $tbl_new);

$cmd = $cfg->val('tickets', 'escalated');
$csvstring = `perl $home/$cmd`;
my $tbl_escalated = Text::CSV::Slurp->load(string=>$csvstring, sep_char=>';');
$template->param('escalated' => $tbl_escalated);

# status fields
my @sts_fields;
for my $item ($cfg->Parameters('status')) {
    $cmd = $cfg->val('status',$item);
    my $value = `perl $home/$cmd`;
    # danger and success correspond to bootstrap CSS class names
    if(index($cmd, "srvc") > 0) {
        $value = ( $value == 0 ) ? 'success' : 'danger';
    }
    else {
        $value = ( $value == 0 ) ? 'danger' : 'success';
    }
    push @sts_fields, {'Item' => $item, 'Value' => $value};
}
$template->param('status' => \@sts_fields);

# counter fields
my @counter_fields;
for my $item ($cfg->Parameters('counter')) {
    $cmd = $cfg->val('counter',$item);
    my $value = `perl $home/$cmd`;
    push @counter_fields, {'Item' => $item, 'Value' => $value};
}
$template->param('counter' => \@counter_fields);

# time fields
my @time_fields;
for my $item ($cfg->Parameters('time')) {
    $cmd = $cfg->val('time',$item);
    chomp(my $time_left = `perl $home/$cmd`);
    my $time_spent = 100 - $time_left;
    push @time_fields, {'Item' => $item, 'left' => $time_left, 'spent' => $time_spent};
}
$template->param('time' => \@time_fields);

open(my $fh, '>', $filename) or die "Не могу открыть '$filename' $!";
print $fh $template->output();
close $fh;

sleep 30
}