#!/usr/bin/perl

use strict;
use JSON::RPC::Client;
use Net::Graphite;
use Data::Dumper;
use utf8;
use open qw(:std :utf8);

my $debug = 1;
my $id = 23;     # ищем триггеры связанные с банкоматами
my $client = new JSON::RPC::Client;
my $url = 'http://zabbix.com/zabbix/api_jsonrpc.php';
my $authID;
my $response;
my $number_of_triggers;
my @triggerids;
my @hostids;

my $days = shift or die "Необходим аргумент - количество дней\n";

my $json = {
    jsonrpc => "2.0",
    method => "user.login",
    params => {
        user => "zabbix",
        password => "zabbix"
    },
    id => 1
};
undef $response;
while ( ! defined $response ) {
    $response = $client->call($url, $json);
}
die "Authentication failed\n" unless $response->content->{'result'};
$authID = $response->content->{'result'};

$json = {
    jsonrpc=> '2.0',
    method => 'trigger.get',
    params => {
        output => ['triggerid','lastchange','description'],
        groupids => $id,
        sortfield => 'lastchange',
        monitored => 1,
        filter => {
            value => 1,
        }
    },
    id => 2,
    auth => "$authID",
};
undef $response;
while ( ! defined $response ) {
    $response = $client->call($url, $json);
}
die "trigger.get failed\n" unless $response->content->{result};

foreach my $trigger (@{$response->content->{result}}) {
    if (time - $trigger->{lastchange} > 86400 * $days) {
        
        my $age = time - $trigger->{lastchange};
        print convert_time($age).",";
        
        $json = {
            jsonrpc=> '2.0',
            method => 'hostinterface.get',
            params => {
                output => ['ip','dns'],
                sortfield => 'dns',
                triggerids => $trigger->{triggerid},
            },
            id => 3,
            auth => "$authID",
        };
        undef $response;
        while ( ! defined $response ) {
            $response = $client->call($url, $json);
        }
        die "hostinterface.get failed\n" unless $response->content->{result};

        foreach my $hostint (@{$response->content->{result}}) {
            print $hostint->{dns}.",".$hostint->{ip}.",";
        }

        $json = {
            jsonrpc=> '2.0',
            method => 'hostgroup.get',
            params => {
                output => ['name'],
                triggerids => $trigger->{triggerid},
            },
            id => 4,
            auth => "$authID",
        };
        undef $response;
        while ( ! defined $response ) {
            $response = $client->call($url, $json);
        }
        die "hostgroup.get failed\n" unless $response->content->{result};

        foreach my $hostgrp (@{$response->content->{result}}) {
            print $hostgrp->{name}."\n" if ( 0 <= index($hostgrp->{name}, "atm"));
        }
    }
}


sub convert_time {
    my $time = shift;
    my $days = int($time / 86400);
    $time -= ($days * 86400);
    my $hours = int($time / 3600);
    $time -= ($hours * 3600);
    my $minutes = int($time / 60);
    my $seconds = $time % 60;

    $days = $days < 1 ? '' : $days .'д ';
    $hours = $hours < 1 ? '' : $hours .'ч ';
    $minutes = $minutes < 1 ? '' : $minutes . 'м ';
    $time = $days . $hours . $minutes . $seconds . 'с';
    return $time;
}