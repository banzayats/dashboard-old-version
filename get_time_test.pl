#!/usr/bin/perl

use strict;
use warnings;
use Time::Local;
use POSIX;
use Data::Dumper;

my @time = localtime();
my @end_of_day;
my $step;

if ($time[6] == 5) {
    @end_of_day = (0, 30, 16, $time[3], $time[4], $time[5]);
    $step = 288;
} else {
    @end_of_day = (0, 45, 17, $time[3], $time[4], $time[5]);
    $step = 333;
}

my $time = timelocal(@time);
my $end_of_day = timelocal(@end_of_day);

my $time_left = floor(($end_of_day - $time)/$step);

if ( $time_left < 0) {
    print 0;
}

if ( $time_left > 100) {
    print 100;
}
else {
    print $time_left."\n";
}

