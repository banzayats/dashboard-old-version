#!/usr/bin/perl

use Modern::Perl;
use Data::Dumper;
use HTTP::Date;
use SOAP::Lite( 'autodispatch', proxy => 'http://otrs.com/otrs/rpc.pl' );

my $User = 'user';
my $Pw   = 'pa$$w0rd';
my $RPC = Core->new();

## Поиск заявок пользователя
my $TicketOpen = $RPC->Dispatch( $User, $Pw, 'TicketObject', 'TicketSearch', Result => 'COUNT', QueueIDs => [5, 10], StateType => 'Open', UserID => 1);
my $TicketNew = $RPC->Dispatch( $User, $Pw, 'TicketObject', 'TicketSearch', Result => 'COUNT', QueueIDs => [5, 10], StateType => 'New', UserID => 1);
my @TicketSearchNew = $RPC->Dispatch( $User, $Pw, 'TicketObject', 'TicketSearch', Result => 'ARRAY', QueueIDs => [5, 10], StateType => 'New', UserID => 1);

if ($TicketNew > 0) {
	say "Created;Title;TicketID";
	foreach (@TicketSearchNew){
		my %Ticket = $RPC->Dispatch( $User, $Pw, 'TicketObject', 'TicketGet', , TicketID => $_, UserID => 1, StateType => 'New');
		say join(';', $Ticket{Created}, $Ticket{Title}, $Ticket{TicketID} ) if ($Ticket{CreateTimeUnix} > time()-86400*7);
	}
} else {
    say "Created;Title;TicketID";
    say "-;-;-";
}
